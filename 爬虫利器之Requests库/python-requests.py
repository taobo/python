import requests
import json
import sys
import io

# gb18030
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')

# 基本get请求
# r = requests.get('http://cuiqingcai.com')
# print(type(r))
# print(r.status_code)
# print(r.encoding)
# print(r.cookies)

# # 如果想要加参数，可以利用params参数
# payload = {'key1': 'value1', 'key2': 'value2'}
# r = requests.get('http://www.baidu.com', payload)
# print(r.url)  # http://www.baidu.com/?key1=value1&key2=value2


# 如果想请求JSON文件，可以利用 json() 方法解析
# 其中a.json方法是要放在远端服务器中，本地是无法请求的。
# r = requests.get("a.json")
# print(r.text)
# print(r.json())

# 如果想获取来自服务器的原始套接字响应，可以取得 r.raw 。
# 不过需要在初始请求中设置 stream=True 。
# r = requests.get('https://www.baidu.com', stream=True)
# # <requests.packages.urllib3.response.HTTPResponse object at 0x00000000030AB3C8>
# print(r.raw)

# 如果想添加headers，可以传headers参数

# payload = {'key1': 'value1', 'key2': 'value2'}
# headers = {'content-type': 'application/json'}
# r = requests.get("http://httpbin.org/get", params=payload, headers=headers)
# print(r.url)
# 通过headers参数可以增加请求头中的headers信息

# ===================post==================
# 基本post请求

# payload = {'key1': 'value1', 'key2': 'value2'}
# r = requests.post("http://httpbin.org/post", data=payload)
# print(r.text)

# 有时候我们需要传送的信息不是表单形式的，
# 需要我们传JSON格式的数据过去，
# 所以我们可以用 json.dumps() 方法把表单数据序列化。
# url = 'http://httpbin.org/post'
# payload = {'some': 'data'}
# r = requests.post(url, data=json.dumps(payload))
# print(r.text)

# 如果想要上传文件，那么直接用 file 参数即可
# 新建一个 a.txt 的文件，内容写上 Hello World!
# url = 'http://httpbin.org/post'
# files = {'file': open('a.txt', 'rb')}
# r = requests.post(url, files=files)  # 返回我们传入的参数
# print(r.text)

# requests 是支持流式上传的，这允许你发送大的数据流或文件而无需先把它们读入内存。
# 要使用流式上传，仅需为你的请求体提供一个类文件对象即可

# with open('massive-body') as f:
#     requests.post('http://some.url/streamed', data=f)

# ---------------Cookies------------------
# 如果一个响应中包含了cookie，那么我们可以利用 cookies 变量来拿到
# url = 'http://www.baidu.com'
# r = requests.get(url)
# print(r.cookies)
# print(r.cookies['BDORZ'])

# 可以利用cookies变量来向服务器发送cookies信息

# url = 'http://httpbin.org/cookies'
# cookies = dict(cookies_are='working')
# r = requests.get(url, cookies=cookies)
# print(r.text)

# -----------------超时配置----------------
# requests.get('http://github.com', timeout=0.001)
# timeout仅对连接过程有效，与响应体的下载无关。

# -----------------会话对象----------------
# 在以上的请求中，每次请求其实都相当于发起了一个新的请求。
# 也就是相当于我们每个请求都用了不同的浏览器单独打开的效果。
# 也就是它并不是指的一个会话，即使请求的是同一个网址。比如：
# requests.get('http://httpbin.org/cookies/set/sessioncookie/123456789')
# r = requests.get('http://httpbin.org/cookies')
# print(r.text)  # "cookies": {} 说明这两段请求不是一个会话。

# s = requests.Session()
# s.get('http://httpbin.org/cookies/set/sessioncookie/123456789')
# r = s.get('http://httpbin.org/cookies')
# print(r.text)
# 上面请求了两次，一次是设置cookies，一次获得cookies
# 可以看出成功获取到cookies了，这就是建立一个会话的作用。

# 既然会话是一个全局的变量，那么我们肯定可以用来全局的配置了。

# s = requests.Session()
# s.headers.update({'x-test': 'true'})
# r = s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
# print(r.text)
# 通过 s.headers.update 方法设置了 headers 的变量。
# 然后我们又在请求中设置了一个 headers，那么会出现什么结果？
# 很简单，两个变量都传送过去了。

# 如果get方法传的headers 同样也是 x-test 呢？
# r = s.get('http://httpbin.org/headers', headers={'x-test': 'false'})
# print(r.text)
# 嗯，它会覆盖掉全局的配置,也就是后面设置的会覆盖全面的。

# 那如果不想要全局配置中的一个变量了呢？很简单，设置为 None 即可
# r = s.get('http://httpbin.org/headers', headers={'x-test': None})
# print(r.text)

# ---------------SSL证书验证-----------------
# Requests可以为HTTPS请求验证SSL证书，就像web浏览器一样。
# 要想检查某个主机的SSL证书，你可以使用 verify 参数

# r = requests.get('https://github.com', verify=True)
# print(r.text)

# ---------------代理-----------------------
# 如果需要使用代理，你可以通过为任意请求方法提供 proxies 参数来配置单个请求。
proxies = {
    "https": "http://41.118.132.69:4433"
}
r = requests.post("http://httpbin.org/post", proxies=proxies)
print(r.text)
