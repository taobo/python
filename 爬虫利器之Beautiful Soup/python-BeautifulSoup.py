from bs4 import BeautifulSoup

html = """
<html>
<head>
<title>
The Dormouse's story
</title>
</head>
<body>
<p class="title" name="dromouse"><b>The Dormouse's story</b></p>
<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1"><!-- Elsie --></a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>
<p class="story">...</p>
"""

soup = BeautifulSoup(html, "html.parser")
print(soup.prettify())

# 我们还可以用本地html文件来创建对象
# soup = BeautifulSoup(open('index.html'), "html.parser")
# print(soup.prettify())
print(type(soup.title))
print("title：", soup.title)
print("head:", soup.head)
print("a:", soup.a)
print("p:", soup.p)
print("-------------------------------")
# 对于 Tag，它有两个重要的属性，是 name 和 attrs，下面我们分别来感受一下
print(soup.name)
print(soup.head.name)
print(soup.p.attrs)

print(soup.p['class'])

# 我们可以对这些属性和内容等等进行修改，例如
# soup.p['class'] = "newClass"
# print(soup.p)

# 还可以对这个属性进行删除
# del soup.p['class']
# print(soup.p)

# ================NavigableString=================
# 既然我们已经得到了标签的内容，那么问题来了，我们要想获取标签内部的文字怎么办呢？
# 很简单，用 .string 即可，例如
print(soup.p.string)
# print(type(soup.p.string)) #<class 'bs4.element.NavigableString'>

# ================BeautifulSoup===================
# BeautifulSoup 对象表示的是一个文档的全部内容.
# 大部分时候,可以把它当作 Tag 对象，是一个特殊的 Tag，我们可以分别获取它的类型，名称，以及属性来感受一下
print("=============BeautifulSoup===============")
print(type(soup.name))
print(soup.name)
print(soup.attrs)

# ================Comment===================
# Comment 对象是一个特殊类型的 NavigableString 对象，
# 其实输出的内容仍然不包括注释符号，但是如果不好好处理它，
# 可能会对我们的文本处理造成意想不到的麻烦
print("==================comment=================")
print(soup.a)
print(soup.a.string)  # 会把注释给过滤掉
print(type(soup.a.string))
